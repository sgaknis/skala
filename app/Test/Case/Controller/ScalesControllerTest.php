<?php
App::uses('ScalesController', 'Controller');

/**
 * ScalesController Test Case
 *
 */
class ScalesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.scale',
		'app.step'
	);

}
