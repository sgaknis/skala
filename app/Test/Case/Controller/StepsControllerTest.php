<?php
App::uses('StepsController', 'Controller');

/**
 * StepsController Test Case
 *
 */
class StepsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.step'
	);

}
