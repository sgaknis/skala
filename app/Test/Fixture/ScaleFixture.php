<?php
/**
 * ScaleFixture
 *
 */
class ScaleFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'scale';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		's1' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's2' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's3' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's4' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's5' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's6' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		's7' => array('type' => 'integer', 'null' => true, 'default' => null, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			's1_idx' => array('column' => 's1', 'unique' => 0),
			's2_idx' => array('column' => 's2', 'unique' => 0),
			's3_idx' => array('column' => 's3', 'unique' => 0),
			's4_idx' => array('column' => 's4', 'unique' => 0),
			's5_idx' => array('column' => 's5', 'unique' => 0),
			's6_idx' => array('column' => 's6', 'unique' => 0),
			's7_idx' => array('column' => 's7', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			's1' => 1,
			's2' => 1,
			's3' => 1,
			's4' => 1,
			's5' => 1,
			's6' => 1,
			's7' => 1
		),
	);

}
