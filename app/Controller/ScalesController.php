<?php
App::uses('AppController', 'Controller');
/**
 * Scales Controller
 *
 * @property Scale $Scale
 */
class ScalesController extends AppController {

var $uses = array('Scale', 'Step','Note');

  public function index() {
    //assume:: gets all the records from db table scales and saves to variable $scales
     $this->set('scales', $this->Scale->find('all'));
  }

  public function view($id = null) {

      $sc_vals = $this->Scale->returnScaleVal($id);
      $this->Scale->id = $id;

        if (!$this->Scale->exists()) {
          throw new NotFoundException(__('Invalid scale'));
        }

      $this->set('scales', $this->Scale->read(null, $id));
      $this->set('scale_values', $sc_vals[0]);

      $setKey = 1;
      $keynote = $this->Note->returnKeyNote($setKey);
      $notes = $this->Note->calculateNotes($setKey, $id);
      $this->set('notes', $notes);
      $this->set('keynote', $keynote);
     }
}