<?php
App::uses('AppController', 'Controller');
/**
 * Notes Controller
 *
 * @property Note $Note
 */
class NotesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Note->recursive = 0;
		$this->set('notes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Note->id = $id;
		if (!$this->Note->exists()) {
			throw new NotFoundException(__('Invalid note'));
		}
		$this->set('note', $this->Note->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	// public function add() {
	// 	if ($this->request->is('post')) {
	// 		$this->Note->create();
	// 		if ($this->Note->save($this->request->data)) {
	// 			$this->flash(__('Note saved.'), array('action' => 'index'));
	// 		} else {
	// 		}
	// 	}
	// }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	// public function edit($id = null) {
	// 	$this->Note->id = $id;
	// 	if (!$this->Note->exists()) {
	// 		throw new NotFoundException(__('Invalid note'));
	// 	}
	// 	if ($this->request->is('post') || $this->request->is('put')) {
	// 		if ($this->Note->save($this->request->data)) {
	// 			$this->flash(__('The note has been saved.'), array('action' => 'index'));
	// 		} else {
	// 		}
	// 	} else {
	// 		$this->request->data = $this->Note->read(null, $id);
	// 	}
	// }

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
// 	public function delete($id = null) {
// 		if (!$this->request->is('post')) {
// 			throw new MethodNotAllowedException();
// 		}
// 		$this->Note->id = $id;
// 		if (!$this->Note->exists()) {
// 			throw new NotFoundException(__('Invalid note'));
// 		}
// 		if ($this->Note->delete()) {
// 			$this->flash(__('Note deleted'), array('action' => 'index'));
// 		}
// 		$this->flash(__('Note was not deleted'), array('action' => 'index'));
// 		$this->redirect(array('action' => 'index'));
// 	}
}
