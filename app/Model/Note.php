<?php
App::uses('AppModel', 'Model');
/**
 * Note Model
 *
 */

class Note extends AppModel {


/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'note';

  public function returnKeyNote($k) {
    $key = $this->read(null, $k);
    $kid = $key['Note']['id'];
    if ($key['Note']['valup'] == null) {
      $keynote = $key['Note']['valdown'];
    }else {
      $keynote = $key['Note']['valup'];
    }
    return $keynote;
  }

  public function calculateNotes($k, $id) {

    //set key note
    $key = $this->read(null, $k);
    $kid = $key['Note']['id'];

    //get the scale steps ids
    $getscale_ids = ClassRegistry::init('Scale')->returnScaleId($id);
    $step_ids = $getscale_ids[0];
    //save step ids to new array
      foreach ($step_ids as $key => $val ) {
        $step[] =  $val['id'];
      }
      //set the first note
      $i = 1;
      $n[$i] = $kid;
      //create notes array
      foreach ($step as $key => $val ) {
        $n[$i+1] = $n[$i] + $val;
          //check if notes go over 12
          if ($n[$i+1] > 12) {
             $n[$i+1] = $n[$i+1] -12;
          }
        $i++;
      }
      //get all notes
      $notes = $this->find('all');
      $length = count($n);
      //set note array for controller
      for ($i = 1; $i<$length; $i++){
        $note1_id = $n[$i];
        $note2_id = $n[$i+1];
        $dif = $note2_id - $note1_id;
        if ($note2_id<$note1_id) {
          $dif = ($note2_id+12) - $note1_id;
        }
        if ($dif == 1) { // print as semitone
          $note[$i] =  $notes[$note2_id-1]['Note']['valdown'];
          if ($notes[$note2_id-1]['Note']['valdown'] == null) {
            $note[$i] = $notes[$note2_id-1]['Note']['valup'];
          }
        }
        elseif ($dif == 2) {// print as 3semitone
          $note[$i] = $notes[$note2_id-1]['Note']['valup'];
        }
        elseif ($dif > 2) { // print as tone
          $note[$i] = $notes[$note2_id-1]['Note']['valup'];
        }
      }
  //return array to controller
  return $note;
  }

}


