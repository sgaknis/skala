<?php
App::uses('AppModel', 'Model');
/**
 * Step Model
 *
 */
class Step extends AppModel {
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'step';

  public function returnStep(){
    return $this->find('all');
  }

}
