<?php
App::uses('AppModel', 'Model');
/**
 * Scale Model
 *
 * @property Step $Step
 */
class Scale extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'scale';

  public function returnScaleVal($id) {
      return $this->find('all', array(
        'fields' => array('s1.val', 's2.val', 's3.val', 's4.val', 's5.val', 's6.val', 's7.val'),
        'joins' => array(array('table' => 'step','alias' => 's1','type' => 'inner','conditions' =>array('Scale.s1 = s1.id')),
          array('table' => 'step','alias' => 's2','type' => 'inner','conditions' =>array('Scale.s2 = s2.id')),
          array('table' => 'step','alias' => 's3','type' => 'inner','conditions' =>array('Scale.s3 = s3.id')),
          array('table' => 'step','alias' => 's4','type' => 'inner','conditions' =>array('Scale.s4 = s4.id')),
          array('table' => 'step','alias' => 's5','type' => 'inner','conditions' =>array('Scale.s5 = s5.id')),
          array('table' => 'step','alias' => 's6','type' => 'inner','conditions' =>array('Scale.s6 = s6.id')),
          array('table' => 'step','alias' => 's7','type' => 'inner','conditions' =>array('Scale.s7 = s7.id'))
        ),
        'conditions' => array('Scale.id = '.$id.'')
        ));
  }

  public function returnScaleId($id) {
      return $this->find('all', array(
        'fields' => array('s1.id', 's2.id', 's3.id', 's4.id', 's5.id', 's6.id', 's7.id'),
        'joins' => array(array('table' => 'step','alias' => 's1','type' => 'inner','conditions' =>array('Scale.s1 = s1.id')),
          array('table' => 'step','alias' => 's2','type' => 'inner','conditions' =>array('Scale.s2 = s2.id')),
          array('table' => 'step','alias' => 's3','type' => 'inner','conditions' =>array('Scale.s3 = s3.id')),
          array('table' => 'step','alias' => 's4','type' => 'inner','conditions' =>array('Scale.s4 = s4.id')),
          array('table' => 'step','alias' => 's5','type' => 'inner','conditions' =>array('Scale.s5 = s5.id')),
          array('table' => 'step','alias' => 's6','type' => 'inner','conditions' =>array('Scale.s6 = s6.id')),
          array('table' => 'step','alias' => 's7','type' => 'inner','conditions' =>array('Scale.s7 = s7.id'))
        ),
        'conditions' => array('Scale.id = '.$id.'')
        ));
  }




/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

// /**
//  * hasMany associations
//  *
//  * @var array
//  */
// 	public $hasMany = array(
// 		'Step' => array(
// 			'className' => 'Step',
// 			'foreignKey' => 'id',
// 			'dependent' => false,
// 			'conditions' => '',
// 			'fields' => '',
// 			'order' => '',
// 			'limit' => '',
// 			'offset' => '',
// 			'exclusive' => '',
// 			'finderQuery' => '',
// 			'counterQuery' => ''
// 		)
// 	);

}
